class Motob():
    def __init__(self, motors):
        self.motors = motors
        self.value = []

    def update(self, value):
        print('Update')
        print(value)
        # Receive a new motor recommendation, load it into the value slot, and operationalize it. Value is an array of range 2
        self.value = value
        self.operationalize()

    def operationalize(self):
        print('Operation')
        if self.value[0] == "L":
            self.motors.stop()
            self.motors.left(self.value[1]/100)
        elif self.value[0] == "R":
            self.motors.stop()
            self.motors.right(self.value[1]/100)
        elif self.value[0] == "F":
            # Drive forwards with speed self.value[1]
            self.motors.forward(self.value[1]/100)
        elif self.value[0] == "B":
            # Drive backwards with speed self.value[1]
            self.motors.backward(self.value[1]/100)
        elif self.value[0] == "BL":
            s = int(self.motors.max * self.value[1]/100)
            self.motors.set_left_dir(1)
            self.motors.set_left_speed(0)
            self.motors.set_right_dir(0)
            self.motors.set_right_speed(s)
        elif self.value[0] == "BR":
            s = int(self.motors.max * self.value[1] / 100)
            self.motors.set_left_dir(0)
            self.motors.set_left_speed(s)
            self.motors.set_right_dir(1)
            self.motors.set_right_speed(0)
        elif self.value[0] == "S":
            self.motors.stop()