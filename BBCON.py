import time


class BBCON():
    def __init__(self, behaviors, sensobs, motob, arbitrator):
        self.behaviors = behaviors
        self.active_behaviors = []
        self.sensobs = sensobs
        self.motob = motob
        self.arbitrator = arbitrator

    def add_behavior(self, behavior):
        self.behaviors.append(behavior)

    def add_sensob(self, sensob):
        self.sensobs.append(sensob)

    def activate_behavior(self, behavior):
        if behavior not in self.active_behaviors:
            self.active_behaviors.append(behavior)

    def deactive_behavior(self, behavior):
        if behavior in self.active_behaviors:
            self.active_behaviors.remove(behavior)

    def run_one_timestep(self):
        self.active_behaviors = self.behaviors
        for sensob in self.sensobs:
            sensob.update()

        for behavior in self.behaviors:
            behavior.update()

            if behavior.active_flag:
                self.activate_behavior(behavior)

            else:
                self.deactive_behavior(behavior)

        recomedation = self.arbitrator.choose_action(self.active_behaviors)
        print(self.active_behaviors)
        self.motob.update(recomedation)
        # for i in range(self.motobs.length):
        #     self.motobs[i].update(recomedation[i])

        time.sleep(0.5)

        for sensob in self.sensobs:
            sensob.reset()
