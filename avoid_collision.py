from behaviour import Behaviour
from ultrasonic import Ultrasonic


class avoidCollision(Behaviour):
    def __init__(self, bbcon, sensob):
        super().__init__(bbcon, sensob)
        self.ultrasonic = Ultrasonic()
        self.priority = 1
        self.match_degree = 1
        self.weight = 0
        self.stop = 0

    def consider_activation(self):
        self.active_flag = True

    def consider_deactivation(self):
        self.active_flag = True

    def sense_and_act(self):
        self.ultrasonic.update()
        distance = self.ultrasonic.get_value()
        if distance < 10:
            if self.stop < 5:
                self.motorRecs = ('S', 25)
                self.stop += 1
            else:
                self.motorRecs = ('BL', 25)
                self.stop = 0
        else:
            self.motorRecs = ('F', 25)