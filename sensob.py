import camera
import ultrasonic
import irproximity_sensor


class Sensob:

    def __init__(self, sensor):
        self.value = None
        self.sensor = sensor

    def update(self):
        self.sensor.update()
        self.value = self.sensor.get_value()
        return self.value

    def reset(self):
        self.value = None

