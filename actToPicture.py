from behaviour import Behaviour
import BBCON


class actToPicture(Behaviour):

    def __init__(self, bbcon, sensob):
        super().__init__(bbcon, sensob)




    def consider_activation(self):
        self.active_flag = True

    def consider_deactivation(self):
        self.active_flag = True




    def update(self):
        self.consider_activation()
        if not self.active_flag:
            self.weight = 0
            return
        self.sense_and_act()
        self.weight = self.priority * self.match_degree



    def sense_and_act(self):
        image = self.sensob.sensor.value #ImageObj
        r,g,b = image.getpixel((32,32))
        if g == max(r,g,b):
            self.motorRecs = ('F', 25)
            self.priority = 2
            self.match_degree = 1
            print('GRONN')
        elif r == max(r,g,b):
            self.motorRecs = ('S', 25)
            self.priority = 2
            self.match_degree = 1
            print('ROD')

        else: self.priority=2




