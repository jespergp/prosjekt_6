from BBCON import BBCON
from sensob import Sensob
from motob import Motob
import behaviour
from arbitrator import Arbitrator
from camera import Camera
from reflectance_sensors import ReflectanceSensors
from irproximity_sensor import IRProximitySensor
from motors import Motors
from actToPicture import actToPicture
from follow_lines import behavior_follow
from avoid_collision import avoidCollision
from zumo_button import ZumoButton
import time


if __name__ == '__main__':
    camera = Sensob(Camera())
    ultrasonic = Sensob(ReflectanceSensors())
    ir = Sensob(IRProximitySensor())
    print(ultrasonic)
    motors = Motors()
    motob = Motob(motors)
    arbitrator = Arbitrator()



    BBCON = BBCON([], [camera, ultrasonic, ir], motob, arbitrator)
    follow_lines = behavior_follow(BBCON, ir)
    act_to_picture = actToPicture(BBCON, camera)
    avoid_collision = avoidCollision(BBCON, ultrasonic)
    behaviors = [act_to_picture, avoid_collision, follow_lines]
    print(behaviors)
    BBCON.behaviors = behaviors

    zumo_button = ZumoButton()

    zumo_button.wait_for_press()
    motors.forward(0.5,dur=1000)
    # motors.forward(1, dur=300)
    # BBCON.motob.value = ("F", 25)
    # print("Fatt verdi")
    # BBCON.motob.operationalize()
    # print("KJOERER")

    while True:
        BBCON.run_one_timestep()