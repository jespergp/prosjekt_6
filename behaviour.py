

class Behaviour:

    def __init__(self, bbcon, sensob):
        self.bbcon = bbcon
        self.sensob = sensob
        self.motorRecs = None
        self.active_flag = False
        self.halt_request = None
        self.priority = 0
        self.match_degree = 0
        self.weight = 0


    def consider_deactivation(self):
        pass

    def consider_activation(self):
        pass

    def update(self):
        if self.active_flag:
            self.consider_deactivation()
        else:
            self.consider_activation()

        self.consider_activation()
        self.sense_and_act()

        self.weight = self.priority*self.match_degree


    def sense_and_act(self):
        pass





