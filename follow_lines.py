from reflectance_sensors import ReflectanceSensors
from behaviour import Behaviour




class behavior_follow(Behaviour):

    def __init__(self, bbcon, sensob): #kaller objekt av sensob klassen
        super().__init__(bbcon, sensob)
        self.reflectance_sensors = ReflectanceSensors()
        self.priority = 1  #vi må velge selv
        self.threshold = 0.3
        self.match_degree = 1




    def sense_and_act(self):
        #initialsierer hva hver enkelt motor skal gjøre, ventre og høyre motor
        # self.motorRecs = ('S', 0)

        self.reflectance_sensors.update()
        list_values = self.reflectance_sensors.get_value()  #oppdaterte sensor verdier

        if list_values[0] < self.threshold or list_values[1] < self.threshold:
            self.match_degree = 1
            self.motorRecs = ('R', 25)
            print('Hoyresving')
        elif list_values[4] < self.threshold or list_values[5] < self.threshold:
            self.match_degree = 1
            self.motorRecs = ('L', 25)
            print('Venstresving')
        else:
            self.match_degree = 0.3
            self.motorRecs = ('F', 25)
            print('FORROVER')




    def consider_activation(self):
        self.active_flag = True


    def consider_deactivation(self):
        self.active_flag = True
