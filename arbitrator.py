import random

class Arbitrator:

    def __init__(self):
        #har ikke noe å initialisere
        pass



    #velger en stokastisk løsning
    def choose_action(self, active_behaviors):

        #har en range fra 0 til 2.0, hvis b1 har vekt 0.8 så får den range 0 til 0.8, b2 med vekt 0.5 får da range 0.8 til 1.3
        all_behaviors = []
        total_weight = 0
        for current_behavior in active_behaviors:
            all_behaviors.append(total_weight + current_behavior.weight)
            total_weight += current_behavior.weight

        r = random.randint(0, len(active_behaviors)-1) #random.random() gir bare float mellom[0.0,1.0]
        print(r)

        #korresponderende behavior til r vinner:
        for x in range(len(all_behaviors)):
            if r < all_behaviors[x]: #faller innenfor riktig område
                # returnerer en tuppel (motor recommendations, a boolean value)
                return active_behaviors[x].motorRecs  # , active_behaviors[x].halt_request
        # return active_behaviors[r].motorRecs